import { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import { FaEdit } from "react-icons/fa";
import { FaRegTrashAlt } from "react-icons/fa";
import FormRegister from "./Components/FormRegister";
import FormUpdate from "./Components/FormUpdate";

axios.defaults.baseURL = "http://localhost:7000";

function App() {
  const [addSection, SetAddSection] = useState(false);
  const [editSection, SetEditSection] = useState(false);
  const [dataList, setDataList] = useState([]);
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    mobile: "",
    password: "",
  });

  const [formDataEdit, setFormDataEdit] = useState({
    name: "",
    email: "",
    mobile: "",
    _id: "",
  });

  const handleChange = (e) => {
    const { value, name } = e.target;
    setFormData((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };

  // read data from database
  const getFetchData = async () => {
    const data = await axios.get("/");
    console.log(data);
    if (data.data.success) {
      setDataList(data.data.data);
    }
  };
  console.log(dataList);

  useEffect(() => {
    getFetchData();
  }, []);

  // Function to check if the email is valid and all required fields are present
  const isValidFormData = (data) => {
    const { name, email, mobile, password } = data;

    // Check if all required fields are present
    if (!name || !email || !mobile || !password) {
      alert("All fields are required");
      return false;
    }

    // Basic email format validation
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    // Check if email is valid
    if (!emailRegex.test(email)) {
      alert("Please enter a valid email address");
      return false;
    }

    return true;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Check if form data is valid
    if (!isValidFormData(formData)) {
      return;
    }

    // Make an API call to check email uniqueness
    try {
      const checkEmail = await axios.post("/api/check-email", {
        email: formData.email,
      });

      if (!checkEmail.data.success) {
        alert("Email already exists. Please use a different email address.");
        return;
      }

      // If email is unique, proceed with the user creation API call
      const data = await axios.post("/api/create", formData);

      if (data.data.success) {
        SetAddSection(false);
        getFetchData();
        alert(data.data.message);
      }
    } catch (error) {
      console.error("Error during form submission:", error);
      alert("Error submitting the form. Check the console for details.");
      console.log(error.response); // Log the detailed error response
    }
  };

  //delete function
  const handleDelete = async (id) => {
    // Display a confirmation dialog
    const isConfirmed = window.confirm(
      "Are you sure you want to delete this user?"
    );

    // Check if the user confirmed the deletion
    if (isConfirmed) {
      try {
        const data = await axios.delete("/api/delete/" + id);

        if (data.data.success) {
          getFetchData();
          alert(data.data.message);
        }
      } catch (error) {
        console.error("Error during delete operation:", error);
        alert("Error deleting the item. Check the console for details.");
        console.log(error.response); // Log the detailed error response
      }
    }
  };

  const handleUpdate = async (e) => {
    e.preventDefault();
    try {
      const data = await axios.put("/api/update", formDataEdit);
      console.log(data);
      if (data.data.success) {
        getFetchData();
        alert(data.data.message);
        SetEditSection(false);
      }
    } catch (error) {
      console.error("Error during update operation:", error);
      alert("Error updating the user. Check the console for details.");
      console.log(error.response); // Log the detailed error response
    }
  };

  const handleEditChange = (e) => {
    const { value, name } = e.target;
    setFormDataEdit((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };

  const handleEdit = (el) => {
    setFormDataEdit(el);
    SetEditSection(true);
  };

  return (
    <div className="">
      <div className="container">
        <div className="add_btn">
          <h1 className="">Crud Operation Using Mern Stack</h1>
          <button className="btn btn_add" onClick={() => SetAddSection(true)}>
            Add
          </button>
        </div>

        {addSection && (
          //call the formRegister.jsx
          <FormRegister
            handleSubmit={handleSubmit}
            handleChange={handleChange}
            handleClose={() => SetAddSection(false)}
            read={formData}
          />
        )}

        {editSection && (
          //call the formUpdate.jsx
          <FormUpdate
            handleSubmit={handleUpdate}
            handleChange={handleEditChange}
            handleClose={() => SetEditSection(false)}
            read={formDataEdit}
          />
        )}

        {/* displaying data */}
        <div className="tableContainer">
          <table>
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Action</th>
              </tr>
            </thead>

            {dataList.length ? (
              <tbody>
                {dataList.map(({ _id, name, email, mobile }) => (
                  <tr key={_id}>
                    <td>{name}</td>
                    <td>{email}</td>
                    <td>{mobile}</td>
                    <td>
                      <button
                        className="btn btn_edit"
                        onClick={() => handleEdit({ _id, name, email, mobile })}
                      >
                        <FaEdit />
                      </button>
                      <button
                        className="btn btn_delete"
                        onClick={() => handleDelete(_id)}
                      >
                        <FaRegTrashAlt />
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            ) : (
              <p className="noFound">No found data</p>
            )}
          </table>
        </div>
      </div>
    </div>
  );
}

export default App;
