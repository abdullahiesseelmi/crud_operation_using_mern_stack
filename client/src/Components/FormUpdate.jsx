import React from 'react'
import "../App.css";
import { IoMdClose } from 'react-icons/io';

const FormUpdate = ({handleSubmit, handleChange, handleClose, read}) => {


   return (
     <div className="addContainer">
       <form action="" onSubmit={handleSubmit}>
         <div className="close_btn" onClick={handleClose}>
           <IoMdClose />
         </div>
         <label htmlFor="name">Name :</label>
         <input
           type="text"
           name="name"
           id="name"
           onChange={handleChange}
           value={read.name}
         />

         <label htmlFor="email">Email :</label>
         <input
           type="email"
           name="email"
           id="email"
           onChange={handleChange}
           value={read.email}
         />

         <label htmlFor="mobile">Mobile :</label>
         <input
           type="text"
           name="mobile"
           id="mobile"
           onChange={handleChange}
           value={read.mobile}
         />

         <button className="btn btnSave">Update</button>
       </form>
     </div>
   );
}

export default FormUpdate