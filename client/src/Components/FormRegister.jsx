import React from 'react'
import '../App.css'
import { IoMdClose } from 'react-icons/io';

const Form = ({ handleSubmit, handleChange, handleClose, read }) => {
  return (
    <div className="addContainer">
      <form action="" onSubmit={handleSubmit}>
        <div className="close_btn" onClick={handleClose}>
          <IoMdClose />
        </div>
        <label htmlFor="name">Name :</label>
        <input
          type="text"
          name="name"
          id="name"
          onChange={handleChange}
          
        />

        <label htmlFor="email">Email :</label>
        <input
          type="email"
          name="email"
          id="email"
          onChange={handleChange}
     
        />

        <label htmlFor="mobile">Mobile :</label>
        <input
          type="text"
          name="mobile"
          id="mobile"
          onChange={handleChange}
         
        />

        <label htmlFor="password">Password :</label>
        <input
          type="password"
          name="password"
          id="password"
          onChange={handleChange}
        />

        <button className="btn btnSave">Save</button>
      </form>
    </div>
  );
};

export default Form