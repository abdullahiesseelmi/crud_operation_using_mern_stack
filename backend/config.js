const mongoose = require('mongoose');

require("dotenv").config();

//connection function
const dbConnected = async function connectToDatabase() {
    try {
        await mongoose.connect(process.env.MongDb_URL, {

        });
        console.log("Connected to the MongoDB!");

     
    } catch (error) {
        console.error("Connection Failed:", error);
        process.exit(1); // Exit the process if the connection fails
    }
}

//call the function 

module.exports = dbConnected;