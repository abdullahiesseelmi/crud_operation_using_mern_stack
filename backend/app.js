const express = require('express')
const cors = require('cors');

require("dotenv").config();
const app = express()
app.use(cors());
app.use(express.json())

const port = process.env.Port;

const db = require("./config.js")
db()

require("./src/routes/routes")(app)

//server running
app.listen(port, function () {
    console.log(`Server is running.........!!!! ${port}`)
});





//reading
// app.get('/', async (req, res) => {
//     const data = await userModel.find({})
//     res.send({ success: true, data: data })
// });



// //update api
// app.put('/api/update', async (req, res) => {
//     console.log(req.body)
//     const { _id, ...rest } = req.body
//     console.log(rest)
//     const data = await userModel.updateOne({ _id: _id }, rest)
//     res.send({ success: true, message: "Updated successfully", data: data })
// })

// //delete api
// app.delete('/api/delete/:id', async (req, res) => {
//     const id = req.params.id
//     console.log(id)
//     const data = await userModel.deleteOne({ _id: id })
//     res.send({ success: true, message: "deleted succesfully", data: data })
// })




// routes


