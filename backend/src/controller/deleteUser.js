
const userModel = require("../models/userModel")

// //delete api
exports.delete_User = async (req, res) => {
    const id = req.params.id
    console.log(id)
    const data = await userModel.deleteOne({ _id: id })
    res.send({ success: true, message: "deleted succesfully", data: data })
}
