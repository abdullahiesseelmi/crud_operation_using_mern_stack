//reading
const userModel = require("../models/userModel")
exports.read_User = async (req, res) => {
    try {
        const data = await userModel.find().sort({ createdAt: -1})
       return res.send({ success: true, data: data })
    } catch (error) {
       return res.send({ success: false, error })
    }
        
}