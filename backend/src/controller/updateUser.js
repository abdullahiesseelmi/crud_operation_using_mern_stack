const userModel = require("../models/userModel")

// //update api
exports.update_User = async (req, res) => {
    console.log(req.body)
    const { _id, ...rest } = req.body
    console.log(rest)
    const data = await userModel.updateOne({ _id: _id }, rest)
    res.send({ success: true, message: "Updated successfully", data: data })
}
