const userModel = require("../models/userModel")
const bcrypt = require('bcrypt');
//save data to the database api
exports.create_User = async (req, res) => {
    console.log(req.body)
    try {
        const { name, email, mobile, password } = req.body;

        // Check if the email already exists in the database
        const existingUser = await userModel.findOne({ email });

        if (existingUser) {
            return res.status(400).send({ success: false, message: "Email already exists" });
        }

        // Hash the password before saving
        const saltRounds = 10; // Adjust salt rounds as needed
        const hashedPassword = await bcrypt.hash(password, saltRounds);

        const user = new userModel({ name, email, mobile, password: hashedPassword });
        await user.save();

        res.send({ success: true, message: "Inserted Successfully", user: user });


    } catch (error) {
        console.error("Error saving user:", error);
        res.status(500).send({ success: false, message: "Error saving user" });
    }
}

exports.check_Email = async (req, res) => {
    try {
        const { email } = req.body;
        console.log(email)

        // Check if the email already exists in the database
        const existingUser = await userModel.findOne({ email });

        if (existingUser) {
            return res.send({ success: false, message: 'Email already exists' });
        }

        // If email is unique
        res.send({ success: true, message: 'Email is unique' });
    } catch (error) {
        console.error('Error checking email:', error);
        res.status(500).send({ success: false, message: 'Error checking email' });
    }
};