
const mongoose = require('mongoose');
const userShemaData = mongoose.Schema({
    name: String,
    email: String,
    mobile: String,
    password: String
}, {
    timestamps: true
})

const userModel = mongoose.model("user", userShemaData)

module.exports = userModel;