const saveUser = require("../controller/saveUser")
const readUser = require("../controller/readUserData")
const updateUser = require("../controller/updateUser")
const deleteUser = require("../controller/deleteUser")

module.exports = (app) => {
    app.post('/api/create', saveUser.create_User)
    app.get('/', readUser.read_User)
    app.put('/api/update', updateUser.update_User)
    app.delete('/api/delete/:id', deleteUser.delete_User)
    app.post('/api/check-email', saveUser.check_Email)
}